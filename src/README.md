# Welcome

Personal thoughts, creations, experiments, blog.

## Recent content

- [Projects](./projects/index.html)
- I started a long series of a [Flask tutorial](./doc/flask-tutorial/index.html).
  I want this to be a very detailed tutorial with many side notes, not only
  focused on the technical viewpoint
