# Dotfiles

I manage my dotfiles using git, like anybody. But there's a twist:

> No separate local repo and deploy script required, $HOME is your repo

```sh
cd ~
git init
# This is the trick, only expressly added files are tracked.
echo '*' > .gitignore
git add .gitignore
git commit -m "empty dotfiles"
# -f force `git add` despite a gitignore rule
git add -f .whatever .dotfiles .and .config/dirs
git commit -m "some dotfiles"
git remote add origin git@a-remote-somewhere
git push origin master
# whenever you change something in comited files
git commit -am "changes..." && git push origin master
````

On your fresh install

```sh
cd ~
git init
git remote add origin git@a-remote-somewhere
git fetch
git reset origin/master
git restore .
````

Beware not to commit passwords, tokens, GPG keys, though.

You can add a branch for host specific configuration and merge master back to
those from time to time.
