# Simple python events

In [KakFM](../projects/kakfm.md), I needed a simple event system.

I created this.

<!-- literate: chunk :repo=self: :rev=WIP: :path=code/python/simple_events/events.py: ALL -->

This provides

- a decorator to add hooks
- a function to send events.

<!-- literate: chunk :repo=self: :rev=WIP: :path=code/python/simple_events/test_events.py: test_listen_decorator -->

If you need more control and not hard-coding the callbacks,
you may bypass the decorator and use `EventManager.listen` and
`EventManager.unregister` directly:

<!-- literate: chunk :repo=self: :rev=WIP: :path=code/python/simple_events/test_events.py: test_raw_listen -->
