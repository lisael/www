# Summary

- [Welcome](./README.md)

- [Doc](./doc/README.md)
    - [Flask Tutorial](./doc/flask-tutorial/README.md)
        - [Setup](./doc/flask-tutorial/setup.md)

- [Projects](./projects/README.md)

    - [KakFM](./projects/kakfm.md)

    - [Fastidious](./projects/fastidious/README.md)

    - [Literate MdBook](./projects/literate_mdbook/README.md)
 
    - [Rust Postgres logical decoding](./projects/logical_decoding_rs/README.md)
 
    - [Postgres Meili index](./projects/pg_meili_index/README.md)

    - [Rebor](./projects/rebor.md)

    - [Santa](./projects/santa.md)

    - [PyDhall](./projects/pydhall/README.md)

    - [Sithrax](./projects/sithrax/README.md)
        - [Glossary](./projects/sithrax/Glossary.md)
 
    - [Edgedb LSP](./projects/edgedb_lsp/README.md)
 
- [Programing languages](./pl/README.md)

- [Misc](./misc/README.md)
    - [Dotfiles](./misc/dotfiles.md)
    - [Python Simple Events System](./misc/python_events.md)
    - [Git Cheat Sheet](./misc/git_cheatsheet.md)
