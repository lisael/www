# Flask Tutorial

In this (not so much, yet) long series of articles we will build
a non trivial Flask app, starting small, and growing in feature
bloat, as well as in complexity. The technical aspect of this
task, building a Flask app, is quite boring, and there are
dozens of resources on the net for this. However, the aspect
of dealing with the growing complexity is much more interesting,
to me at least. That's why this series insists on code organization,
testing, maintainability and ease of development.

I hope you'll enjoy the reading as much as I enjoy the writting!


