# Setup

This chapter is based on the tag [001_setup](https://gitlab.com/lisael/flask-tutorial/-/tree/001_setup)
of the tutorial repository.

<!-- literate: set :repo=flask-tutorial: :rev=001_setup: :title=yes: -->

In this chapter we setup a minimal development environment to
comfortably develop and run our application. We won't see much of
Flask here, but we'll make solid fundations to build a future-proof
application, following todays professional standards. Don't expect
any new Flask knowledge, but a reminder of development best practices
and my opiniated take on those practices.

> **Content**
> <!-- toc -->

## Project

First create a git repository. Always do. Even for a tutorial. You don't
know beforehand how far it will go. Commit everything everytime.
I believe that git must be a tool to express your imagination. Some
say each and every commit should be ready to be deployed on prod. I
think this method brings a lot of friction in the idea-to-working-code
cycle.

If a clean history is required, create a branch, commit as much as
you want, small commits, unfinished code refactorisation. Squash
everything in 2-3 logical commits before merging.

### Intermezzo: Git

Git is a powerful tool with a complex UI. Whithout a good mental model
of the tool, it's even harder to understand. Here's the very minimal
knowledge every developer should know about git:

1. A commit is not a diff, it's a full copy of all files in the repo
   - Of course, internally, git de-duplicate things, but it's an
     implementation detail.
   - git diffs are built by diffing the full copies of the repo
     contained in the 2 commits.
1. A commit has zero or more parents.
   - parents are pointers to another commit
   - most commit has only one parent
   - Merge commits has more than one.
   - typically only the first commit of a repo has zero parent.
1. A tag is a reference to a commit
1. A branch is a reference to a commit that moves when a child commit
   is created
   - Merging branch B on branch A consists in walking A an B
     ancestors until we find a common ancestor C. Then try to
     apply each diff that leads from C to B on top of A, moving
     A at each step. B doesn't change.
   - Rebasing branch B on branch A consists in walking A an B
     ancestors until we find a common ancestor C. Then try to
     apply each diff that leads from C to B on top of A, **not**
     moving A at all. B becomes the result of this process

The UI is still... perfectible, but it make more sense when your mental
model of the commit is correct.

## Virtualenv

Create a virtualenv in the repo (I used to put them in a
`~/.virtualenvs/` directory too, it's a matter of preference).

    python -m virtualenv venv

## Dependencies

Create a `requirements.txt` file:

<!-- literate: chunk ALL :path=requirements.txt: -->

and install those:

    pip install -r requirements.txt

## Minimal application

Create the most  minimal Flask app in a simple python module:

<!-- literate: chunk ALL :path=trax.py: -->

Let's run this!

```sh
flask

Error: Could not locate a Flask application. You did not provide the
"FLASK_APP" environment variable, and a "wsgi.py" or "app.py" module
was not found in the current directory.

Usage: flask [OPTIONS] COMMAND [ARGS]...

  A general utility script for Flask applications.

  Provides commands from Flask, extensions, and the application. Loads the
  application defined in the FLASK_APP environment variable, or from a wsgi.py
  file. Setting the FLASK_ENV environment variable to 'development' will
  enable debug mode.

    $ export FLASK_APP=hello.py
    $ export FLASK_ENV=development
    $ flask run

Options:
  --version  Show the flask version
  --help     Show this message and exit.

Commands:
  routes  Show the routes for the app.
  run     Run a development server.
  shell   Run a shell in the app context.
```

OK, then. Let's do it.

```sh
export FLASK_APP=trax
export FLASK_ENV=development
flask run

 * Serving Flask app 'trax' (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 298-686-770
```

### Intermezzo: Environment

> That's Nice, but exporting those envvars every time I start working on a project
> is cumbersome and error-prone. Some development environments automatically
> load any `env` or `.env` in the shell of the integrated terminal, or they
> provide a workspace feature with a frontend to set the env of the project.
> I for myself prefer [UNIX for IDE](https://mkaz.blog/code/unix-is-my-ide/),
> and I use [direnv](https://direnv.net/) for this task. `cd`ing into a
> project's repo loads a sensible env. I prefer depending on decades-old
> standards, than tieing my development workflow to an application. To
> each their own!

That said, it's always preferable to explicitely commit development details
(as long as this is no secret or password !). We'll create this file:

<!-- literate: chunk ALL :path=env.dev: -->

Note that the file is not a hidden dotfile, as we want to make it clear to
collaborators that it exists, and has something to do with development,
hence the `.dev`. Furthurmore, I didn't named this `env` so it won't clash
with tools or IDEs that expect such a file.

Speaking of which...

### Intermezzo 2: Gitignore

Those tools and IDEs may pollute your git interractions. Furthermore, the
env file may be the place where the application reads its configuration.
Committing this file is probably a bad idea. You don't want to say "OOPS,
I committed development values and we forgot to deploy the production env
file. Now the production is in debug mode." Let's add this to a `.gitignore`,
as well as python artifacts and the virtualenv

<!-- literate: chunk ALL :path=.gitignore: -->

Some add a bunch of well-know IDE and tools artifacts, this list is hard
to maintain. There is a better solution: tell your collaborators about
`.git/info/exclude`. It's a personal per repo gitignore. It won't be
committed, pushed or shared.

## First Request

Let's try it. I'm going to contradict my old-fart rambling and not
use `curl` :). I prefer the ergonomics of `httpie` which is the best command
line http client, and happens to be a python package. We're going to install
this package, but we want a clean separation of dev tools and runtime
library. Crate a dev-requirents file:

<!-- literate: chunk ALL :path=dev_requirements.txt: -->

Then, as usual:

    pip install -r dev_requirements.txt

`httpie` provides the command `http`:

```sh
export TRAX_URL=http://127.0.0.1:5000/
http $TRAX_URL

HTTP/1.0 200 OK
Content-Length: 11
Content-Type: text/html; charset=utf-8
Date: Fri, 05 Nov 2021 04:34:52 GMT
Server: Werkzeug/2.0.2 Python/3.9.7

Hello, Trax
```
Sweet.

So, let's code!??

Nope. There are some details to get right.

## Collaborating

In this book we create a project in a professional setting. Even
if you want to build a hobby side project, There's
a good chance that at some point another developer works on the
project and has to reproduce a working development. Remember that
you in `$CURRENT_YEAR + 2` **is** another developer.

### Makefile

I've experimented a lot of automation systems for development
environments. Here again your IDE may implement something like
that. Being an old fart, I tend to stick to `make` for this. This
has the advantage of (relative) portability and not imposing
an IDE or OS to your devs. Note that using `make` here bends the
real purpose of the tool, which is... making stuff. Make has
its gotchas, but we're not going to code extensively in the
Makefile. Just create a good enough base and add a target from
time to time during the journey.

Here's a quite general and useful Makefile, I use for my python
projects.

[Makefile@e1b929cf](https://gitlab.com/lisael/flask-tutorial/-/raw/e1b929cf/Makefile)

I wont show or discuss here the details (I keep this to a
dedicated tutorial). Just download it and play with it a little:

    wget https://gitlab.com/lisael/flask-tutorial/-/raw/e1b929cf/Makefile
    make
    TARGETS:
      help                 Show this help
      develop              Create a developent environment
      virtualenv           Create the virtualenv
      requirements         Install or update runtime equirements
      dev_requirements     Install or update runtime equirements
      run                  Run the development server
      smoke-test           Run a smoke test on the root of the service
      clean                remove all build and Python artifacts
      clean-build          remove build artifacts
      clean-pyc            remove Python file artifacts
      reset                Reset all (including the virtualenv)

    VARIABLES (Override with: `VAR_NAME=value make <target>):
      SYS_PYTHON           System python to use to bootstrap the virtualenv (python)
      VENV                 Virtualenv dir (venv)
      SRC_DIR              Main source dir ($(MAKEFILE_DIR)/src)
      SERVER_HOST          Development server host (localhost)
      SERVER_PORT          Development server port (5000)

Make targets have a sensible dependencies tree, so you can without fear run

    # remove everything including your virtualenv
    make reset
    # recreate the venv if needed, re-install the requirements
    # if needed or if the requirements files changed, and run
    # the server:
    make run

### Documentation

We won't talk about user doc here, it's a huge topic, but we must
provide the minimal devleloper's doc to our collaborators. The
Makefile is here to uniformize the dev's experience and should
provide everything they need on a day-to-day basis. So our readme
must at least consist in instructions to install make and all
the tools we will need:

<!-- literate: chunk ALL :path=README.md: -->

