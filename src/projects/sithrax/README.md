# Sithrax

An insane OS for an insecure mobile world.

> NOTE: At the moment this only lives in my head. I try to design a bit
> the target before I start writting code.

<!-- toc -->

## Is it really an OS?

[No](https://en.wikipedia.org/wiki/Betteridge's_law_of_headlines). The
underlying OS is Linux/systemd. However, once the user logs into their session,
they are droped into Sithrax without any possibility to access directly the OS
services, nor the libc. User applications are WASM modules run into Sithrax's
runtime.

So Sithrax is an OS in the sense some say that Emacs is an OS (except it lacks
a usable editor). I did not push the design as far as running Sithrax as PID 1.
Fundamentally it could, that'd be aligned with the philosophy of the project,
and this could even be desirable. But re-inventing an init system is not
something I want to do in my lifetime.

The runtime, can provide services to access OS resources (persistent data
–a.k.a. files–, network, randomness, system clock, peripherials...).
Higher-level resources (configurations, user's address book, http connections,
password store...) are also available through the runtime or applications
provided services. Each ressource is designated by applicative code using
capablities-aware references.

## Capabilitie-aware refe... what?

Capabilites are a way to manage access control on resources.

> If [...] the first thing you’re likely to think of is to ask the
> requestor “who are you?” The fundamental insight of the capabilities
> paradigm is to recognize that this question is the first step on the
> road to perdition.

This quote is from a [very good (not-so-short) introduction to capabilites
](http://habitatchronicles.com/2017/05/what-are-capabilities/). If you've never
heard about capabilities or don't aggree with the quote, pour a pint of coffee,
read the article and come back here to see what we can do.

Back? Nice.

How do we implement capabilities in Sithrax? Let start with an example.

First there is only one agent that is granted all the capabilities on all the
resources of the device, it's the user. Not a user abstraction encoded in the
device, not a UNIX user, but the human person who holds the device.

The Sithrax runtime (Sithrax) also has all those capabilities too, but it's
allowed to hand them to an app only if the user explicitely allows it to do so.

Let say we want to implemeent a weather app (WA). I'm not sure why,
but Gnome Weather uses data that come from the Norwegian Meteorological
Institute (MET).  We'll use the same source. The MET's API is published on
https://thredds.met.no (API).

So. The user opens the weather app. The weather app (WA) needs to access
the API. WA require the capability to read the API. WA ask Sithrax a
read cap on the API. Sithrax shows a message to the user "Weather App
wants to read from https://thredds.met.no <ui-button>Grant</ui-button> <ui-button>Refuse</ui-button>"

The user accepts, Sithrax ask the HTTP service a read capability on API and
gives it to WA. WA can now use this cap to perform a http request on the MET.

This is tedious, isn't it? We can do better.

The first change is that we can ask the user if they grant WA the capability
for ever (e.g. with a tickbox `[ ] Always grant` in the UI). The user then has
to accept once in the lifetime of the system.

That's better, and could work for this simple example, but it's still quite
cumbersome, because every app will ask for a bunch of capabilities, which is
the perfect road to "Hell that.  I always click Grant" behaviour. The other
issue is that the user is most probably clueless about what ressource an
app may legitimely require. As a two decades long programmer and Linux nerd,
I probably don't (even vaguely!) know what my system does with more than the
half of the resources it accesses. But I trust my distribution maintainers, the
security experts and the community to find and remove bad behaviours.

The solution in Sithrax is to attach capabilities to the package. When a user
trusts a repo, they also delegate the grant of capabilities to the package
manager. Everything is logged, there's a frontend to manage app capabilities,
and those can be revoked at any time by the user.

> TODO: add screen sketchs

## Pesistent data resouces, not files

First the radical assertion: Users or apps don't give a s**t about files,
they manipulate data.

In the capablities world this is even more true: we don't hand the application
a path to a file, but a stream of data, with the associated permissions:
`read`, `write`, `read+write`, `delete`. Then the path of the physical place
where the data is stored on disk is pretty useless. The next step is to get
rid of this concept altogether.

Sure at the end of the day, the data may be stored somewhere in Sithrax's
underlying OS's filesystems, but it's not something the user or Sithrax apps
have to bother with.

In Sithrax persistent data resources are content addressable strings (likely a
base64 representation of a sha256 hash of the content), along with metadata:
mimetype, title, creation/modification/access dates, version, language,
user tags, automatic tags like `web download`, `ocr` or `scan`...

Let's explain with an example.

1. The user starts the PDF reader app.
2. They click on <ui-button>Open</ui-button>.
3. The App asks the runtime to ask the user which document they wish to open
4. The runtime displays a data finder to the user with a set of controls
   to search the PDF document they want to read.
5. Once selected, the runtime hands the app a read-only capref on a stream
   containing the PDF data to display.

Point `4.` seems tedious ? Right. We can do a lot better. In `3.`,
the pdf app may pre-fill the filters of the data-finder app, like
`mimetype=application/pdf`.  The data finder may also be smart presenting the
results by frecency (a `least_read × most_read` score –a.k.a. exponential
decay–). Same for the tags, if the data finder knows that most of the time,
when a pdf is requested, it ends up being taged `doc` or `novel`, the tag
chooser widget shows them at the top of the tag list (and tags that don't apply
to any pdf are just hidden), and so on.

It's not that different than what the user ususally do: categorize stuff
into hierarchical folders, each folder name being somewhat of a tag in a
hierarchy of tags. The tagging system embrasses this usage pattern and offers
the possibility to search for the data, using various perspectives. By date,
region, type of document...

A nice side effect of this is that we can transparently build a lot of
cool features on top of this architectcure. The version metadata enables
time-machine like features as well as online and offline bakups.

The metadata database may also refer to data stored on a shared storage,
e.g. a company's private P2P network based on the content-addressable hashs
using a [DHT](https://en.wikipedia.org/wiki/Distributed_hash_table), (IPFS,
possibly). All this remote data is still protected by the capabilites-based
security framework.

## Package management

> TODO: the terminology is not really consitent, "service", "app",
> "provider" are still vague concepts. Add a glossary and stick to those
> terms and definitions in this document.

Once the the DHT infrastructure is up and running, we can implement a
package manager on top of this. The packages are simple data chunks,
refered as their base64 id. The source of trust is the repo p2p network,
where the distribution maintainers curate a metadata database of package name
mapped to a data object. My vision, currently is that an application package
relies on services. Services are (semantically) versioned, and packages must
provide a list of services they depend on. The package manager's task is to
install the requested application package and apps that provide the service
dependencies. Two different apps may rely on a different version of a service,
in this case the package manager installs the needed versions and the runtime
resolves the version to use at runtime.

Because each app is sandboxed and may tract its own dependencies, a given
system may rely on multiple repos. This allows organizations to maintain their
own repos for special configurations, in-house apps...  The only limit is all
the apps must depend on the same runtime implementation. We should provide a
simple repository server to ease the burden of distribution maintainers and
private repos operators.

### Anatomy of a package

> TODO

## Targeted hardware

- PinePhone
- PinePhone pro, when I get one.

## Components

### Underlying OS

- OS (probably a minimal Archlinux)
- Init (systemd)

### Runtime and core services

Those services are hare wired in the runtime.

- Wasm runtime
- Persistent data service
- Display service
- Networking service
- HTTP service
- Camera service
- GPS service
- Mobile broadband service

### Applications

- Calls
- Messages
  - SMS
  - IM (matrix? irc?....)
- Address book
- Calendar
- EMail reader

## Roadmap

### Bootstrap (<-- we are here)

- [ ] Design the big picture
- [ ] Design the runtime and applictions interactions in detail
- [ ] Design the capapbilities system in depth
- [ ] Create a gitlab group, the core's project, setup the CI.

### PoC

- [ ] Code a minimal implementation of the WASM runtime
    - no display
    - [ ] capabilites
- [ ] Persistent data service (only local data)

### MVP

- [ ] Rewrite the core
- [ ] Networking service (DNS, TCP/UDP connections)
- [ ] HTTP service (on top of Networking)
- [ ] Add the install of the underlying OS, and deploy to a pinephone.
- [ ] Advertise and grow a community

### v1.0

- [ ] Distributed persistent data store
- [ ] Package manager
- [ ] Display/UI service
- [ ] Mobile broadband service
- [ ] Address Book app
- [ ] Calls App
- [ ] Messaging App (SMS)

## Related projects and inspirations

- [redshirt](https://tomaka.medium.com/the-future-of-operating-systems-efa31e17d66d) ([repo](https://github.com/tomaka/redshirt))
    - It's a full OS, where apps are WASM modules that run on top of a global runtime
    - They communicate with each other and with the OS using interfaces
    - The package management relies on content addressable files pushed in a DHT
    - The concept of files still exist
    - No capabilites system

- [Capabilities expressed with the Rust type system](https://web.archive.org/web/20180125133227/http://zsck.co/writing/capability-based-apis.html)

## FAQ

### Q: Looks like FushiaOS, isn't it?

#### A: Maybe, yes.

Fushia is a full OS, including hardware drivers, task management,
memory management and so on. That's a lot of work we don't want to do here.

On the userland aspect, it's also a capablities based OS, that aims to
protect os security, the user's data and privacy.

My main grip is that Fushia is developed by a company whose main business
model is to monetize as much as they can the user data they stole. This
impedence mismatch makes me very reluctant to use anything that comes out
of their labs, regardless of the technical merits of the solutions.
