# Postgres Meili index

A rust postgres extension to use [MeiliSearch](https://www.meilisearch.com/)
as an index backend.

## Related work

[ZomboDB](https://www.zombodb.com/) Creates elasticsearch indexes.
