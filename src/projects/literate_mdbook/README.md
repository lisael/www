# Literate MdBook

A mdbook plugin that let one insert snippets from a git repo in a
mdbook. The main use case is writting a step by step tutorial. Each step
is a revision. Snippets are fenced in the git repo by special comments.

This is similar to the [`// ANCHOR` includes](https://rust-lang.github.io/mdBook/format/mdbook.html#including-portions-of-a-file)
but works for all programming languages and accross revisions of a file.

It's implemented in python >= 3.7, without any dependency to allow simple
deployment.

**Contents:**
<!-- toc -->

## Live example

### Source code

These are real life snippets using `literate.py` as an example.

This is the snippet of the rest of this section, in the very source of this page (🤯
Click on the title, I dare you.):

<!-- literate: chunk :repo=self: :rev=HEAD: :path=src/projects/literate_mdbook/README.md: demo -->

<!-- chunk begin demo -->
### Rendered 

<!-- literate: chunk :repo=self: :rev=HEAD: :path=literate.py: my_wip_chunk -->

Set some defaults for the rest of the page:

<!-- literate: set :repo=self: :rev=WIP: :path=literate.py: :title=no: -->
<!-- literate: chunk my_wip_chunk -->

Nevermind, titles are cool

<!-- literate: set :title=yes: -->

<!-- literate: chunk with_ellipsis -->

<!-- literate: chunk :title=no: first_chunk :start_line=21: :end_line=26: -->

Same chunk at various revisions. Note that the title links points to the correct
revision:

<!-- literate: chunk :rev=97e9906: cache_import -->
<!-- literate: chunk :rev=5b0c14a: cache_import -->

The tree directive shows a (sub-)tree of a repo at a revision:

<!-- literate: tree :repo=self: :rev=03d29f1: :path=src/projects: :depth=3: :hidden=yes: -->
<!-- chunk end demo -->

## Features
<!-- literate: set :repo=self: :title=no: :rev=WIP: :path=src/projects/literate_mdbook/README.md: -->

In the mdbook source add special directives:

```md
<!-- literate: ! chunk :repo=tutorial: :rev=chapter_2: :path=src/lib.rs: :lang=rs: first_chunk -->

The `set` directive sets the repo, revision and path for the rest of the
page and even sub-chapters if the current page is a chapter in `SUMMARY.md`.

<!-- literate: ! set :repo=tutorial: :rev=chapter_2: first_chunk -->

After the `set` this is equivalent to the first chunk (sharp-eyes noticed that we got
rid of `:lang=rs:` too, as this is inferred from the file extension)

<!-- literate: ! chunk :path=src/lib.rs: first_chunk -->

However one can force any of those parameters in a subsequent `chunk` directive.

```

Both `chunk` directives read the `src/lib.rs` file in the given repo alias (more on this later...) at
the given revision `chapter_2`.

Then `literate.py` searches for code fences in the file's comments:

```rust,no_run,no_playground
fn main() {
    println!("This line is not rendered in the book");
    // chunk begin first_chunk
    println!("This line is rendered");
    // chunk begin_ellipsis first_chunk
    println!("Those two lines are replaced by '// ... (2 lines)'");
    let _ = 42;
    // chunk end_ellipsis first_chunk
    let _ = "This is imported too.";
    // chunk end first_chunk
    println!("This line is not rendered in the book");
}
```

Results in:

<!-- literate: chunk :lang=rs: first_chunk -->

`chunk` directives may be interleaved in multiple chunks. They don't appear in each other's
results:

```yaml
# chunk begin first
config:
    # chunk begin second
    foo: 42
    # chunk end first
    bar: "baz"
    # chunk end second
```

Results in two chunks:

<!-- literate: chunk :lang=yaml: first -->

<!-- literate: chunk :lang=yaml: second -->

Code fences MUST appear on a single line even in languages that support bloc comments:

```c
#include <stdio.h>

/* chunk
begin not_allowed */

/*
chunk begin not_allowed
*/

/* chunk begin allowed */
```

If the language has two different syntax for bloc comments and inline comments,
the inline syntax should be prefered.

```haskell
id: a -> a
{- chunk begin not_even_supported -}
-- chunk begin supported
```

For languages that don't have comments (looking at you, JSON), the `:start_line=0:`
and `:end_line=12:` keywords may be added to the `<!-- literate: chunk -->` directives
(starting at 0 and both included). This is cumbersome to maintain, though, and
should be avoided when possible.


We can also show the file tree at a given revision (experimental):

<!-- literate: chunk :repo=self: :rev=HEAD: :path=src/projects/literate_mdbook/README.md: tree -->

<!-- chunk begin tree -->
<!-- literate: tree :repo=self: :rev=03d29f1: :path=src/projects: :depth=3: :hidden=yes: -->
<!-- chunk end tree -->

## API

### Chunk

> &lt;!-- literate: chunk :repo=[str]: :rev=[str]: :path=[str]: :lang=[str]: :title=[YES|no]
> :start_line=[int]: "end_line=[int] [chunk_name] -->

#### :repo:

Must be the name of a git repo alias defined in the preprocessor configuration in
the main `book.toml`:

```toml
[preprocessor.literate]
command = "python literate.py"
renderer = ["html"]
repos.self = "."
repos.tutorial = "./tutorial"
```

The repo must be local (at the moment, this may change in the future).

The path of the repo is either an absolute path or a path relative to the book's
root. Using an abolute path is discouraged as it's not reproductible by your
readers, co-authors, and CD pipelines. If the repo is not self the best advice is
to add it as a submodule of the book's repo.

#### :rev:

The revision of the file to include. May be any revision suitable to git:

- short commit sha
- sha
- branch name
- tag name
- relative revision (`HEAD~12`... Should be avoided as it's not reproductible)

The special revision name `WIP` doesn't checkout anything and is handy when
working on a new chapter, even on uncommited changes.

#### :path:

Path of the file, relative to the git root of the repo.

#### :lang:

The programming language of the snippet is inferred from the filename. However, the
inferrence code is fragile and doesn't support a lot of language.

If your target language
is not supported but its inline comment syntax is common enough (`#`, `//`, `--`, `;;`),
just adding `:lang=goofy:` should work and create `\```goofy` code fences in the
markdown source.

Another intersting use case is showing the same snippet (or snippets within a single file)
with multiple highlightings (e.g. for templating languages).

Last, I do use this feature in this page to create the rust and yaml snippets
in the [Features](#features) section. The results are snippet of this page source,
with `:lang=rs:` and `:lang=yaml:` so that `literate.py` knows the comment syntax.

#### :start_line:, :end_line:

Include the snippet between those lines instead of relying on fence comments.

#### :title:

Add a title to the chunk, formated by the repo configuration key `title_format`. Must be `yes` or
`no`. Default is `yes`. `set` directive works for deactivating this in a subtree of the
book which is handy when it's clear that a section is related to a single file.

#### :raw:

`yes|no` if `yes` the text is not included in a markdown code fence. Usefull to
include raw markdown.

### Set

```md
<!-- literate: ! chunk [:repo=&lt;str>:] [:rev=&lt;str>:] [:path=&lt;str>:] [:lang=&lt;str>:] [:title=&lt;YES|no>:] -->
```

Set some params for the rest of the page and sub-chapters. It can be
overriden anytime in `chunk` directives.

### Diff

> Not implemented yet

### Tree

```md
<!-- literate: ! tree :repo=self: :rev=03d29f1: :path=src/projects: :depth=3: :hidden=yes: -->
```

> TODO: docmument arguments

## Name

It's not really [literate programming](https://en.wikipedia.org/wiki/Literate_programming)
as defined by Knuth, as neither a runtime or a compiler compile or run the code interleaved
with the prose. The code resides in a proper package that should be correctly tested.
It has the advantage that it can be applied to any programing language in a similar way,
even multiple languages in the same book or page.

## Install

At the moment it's a PoC for this very book. As such, it's a simple python
module in the book's repo, no tests, no package.

The simple install is to vendor the
[python module](https://gitlab.com/lisael/www/-/raw/master/literate.py)
and add this into your `book.toml`:

<!-- literate: chunk :repo=self: :rev=WIP: :path=book.toml: :title=yes: literate_config -->

Then, only make sure that your build system has python installed.

## Usage

### Setup

> Note: this workflow was never tested. It may not stand the reality trial, and
>       is subject to changes.

Say you want to write a tutorial for your new shiny lib. Start a dedicated project
with this layout (the actual layout may change to adapt to your programming language
packaging rules, and CI/CD system):

```
tutorial
├── book/
├── src/
├── book.toml
├── .gitlab-ci.yaml
└── literate.py
```

Add this in your book.toml:

```toml
[build]
build-dir = "public"

[preprocessor.literate]
command = "python literate.py"
renderer = ["html"]
repos.self = {path=".", title_format="[{path}:{line_nr}](https://gitlab.com/you/tutorial/-/blob/{rev}/{path}#L{line_nr})"}
```

Setup [your CI/CD system](https://rust-lang.github.io/mdBook/continuous-integration.html).

> TODO: explain a git workflow

## Roadmap, possible additions

- [ ] Add an option in `set` direcives to render something like "This section
      describes `[this_file](link://to.source)`" if :path: is set.
- [X] Add a warning when `:rev=WIP:` is used
- [ ] Add a script that change all `:rev=WIP:` to `:rev=HEAD:` if the WIP is commited
      and optionally `:rev=HEAD` to `:rev=<tag_or_sha>:`. Maybe a mdbook renderer ?
- [ ] Add a `WIP_OR_HEAD` revision that warns only if the file is not commited.
- [X] add a tree directive showing the file tree at a given point of history
    - `git ls-tree -r --name-only HEAD src` does the trick
    - The output control is hard, I postpone until the UX is clear
- [ ] Add a diff directive
    - not sure how to fence, though.
        - Diff the whole file (`-U999999`). Problems: the same fence
          comment may be both in ++ and -- separate lines.
        - Diff without any context at all (`-U0`) and let the user choose
          a hunk. Problems: the hunk index may change upon source modifications
    - if a version of the diff is WIP (uncommited), can we use git for this ?
- [ ] Support a lot more languages (at least those [supported by mdbook](https://rust-lang.github.io/mdBook/format/theme/syntax-highlighting.html#supported-languages))
- [ ] Create a proper python package
    - [ ] PyPI packaging
    - [ ] Test on python 3.7 -> 3.10
