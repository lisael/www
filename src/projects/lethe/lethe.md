# Sithrax

An insane OS for an insecure mobile world.

<!-- toc -->

## Is it really an OS?

[No](https://en.wikipedia.org/wiki/Betteridge's_law_of_headlines). The underlying
OS is Linux/systemd. However, once the user logs into their session, they are
droped into Sithrax without any possibility to access directly the OS services,
nor the libc. User applications are WASM modules run into Sithrax's runtime.

So Sithrax is an OS in the sense some say that Emacs is an OS (except it lacks
a usable editor). I did not push the design as far as running Sithrax as PID 1.
Fundamentally it could, that'd be aligned with the philosophy of the project,
and this could even be desirable. But re-inventing an init system is not something
I want to do in my lifetime.

The runtime, can provide services to access OS resources (persistent data
–a.k.a. files–, network, randomness, system clock, peripherials...).
Higher-level resources (configurations, user's address book, http
connections, password store...) are also available through the runtime or
applications provided services. Each ressource is designated by applicative code
using capablities-aware references.

## Capabilitie-aware refe... what?

Capabilites are a way to manage access control on resources.

> If [...] the first thing you’re likely to think of is to ask the
> requestor “who are you?” The fundamental insight of the capabilities
> paradigm is to recognize that this question is the first step on the
> road to perdition.

This quote is from a [very good (not-so-short) introduction to capabilites
](http://habitatchronicles.com/2017/05/what-are-capabilities/). If you've never
heard about capabilities or don't aggree with the quote, pour a pint of
coffee, read the article and come back here to see what we can do.

Back? Nice.

TODO: explain capablities paradigm in Sithrax

## Pesistent data resouces, not files

First the radical assertion: Users or apps don't give a s**t about files,
they manipulate data.

In the capablities world this is even more true: we don't hand the application a path to a file, but
a stream of data, with the associated permissions: `read`, `write`, `read+write`,
`delete`. Then the path of the physical place where the data is stored
on disk is pretty useless. The next step is to get rid of this concept altogether.

Sure at the end of the day, the data may be stored somewhere in Sithrax's underlying
OS's filesystems, but it's not something the user or Sithrax apps have to bother with.

In Sithrax persistent data resources are content addressable strings (likely
a base64 representation of a sha256 hash of the content), along with metadata:
mimetype, title, creation/modification/access dates, version, language, user tags,
automatic tags like `web download`, `ocr` or `scan`...

Let's explain with an example.

1. The user starts the PDF reader app.
2. They click on <ui-button>Open</ui-button>.
3. The App asks the runtime to ask the user which document they wish to open
4. The runtime displays a data finder to the user with a set of controls
   to search the PDF document they want to read.
5. Once selected, the runtime hands the app a read-only capref on a stream
   containing the PDF data to display.

Point `4.` seems tedious ? Right. We can do a lot better. In `3.` the pdf app
may pre-fill the filters of the data-finder app, like `mimetype=application/pdf`.
The data finder may also be smart presenting the results by frecency (a
`least_read × most_read` score –a.k.a. exponential decay–). Same for the tags,
if the data finder knows that most of the time, when a pdf is requested, it ends
up being taged `doc` or `novel`, the tag chooser widget shows them at the top
of the tag list (and tags that don't apply to any pdf are just hidden), and
so on.

It's not that different than what the user ususally do: categorize stuff into
hierarchical folders, each folder name being somewhat of a tag in a hierarchy
of tags. The tagging system embrasses this usage pattern and offers
the possibility to search for the data, using various perspectives. By date,
region, type of document...

A nice side effect of this is that we can transparently build a lot of cool
features on top of this architectcure. The version metadata enables time-machine
like features as well as online and offline bakups.

The metadata database may also refer to data stored on a shared storage,
e.g. a company's private P2P network based on the content-addressable hashs
using a [DHT](https://en.wikipedia.org/wiki/Distributed_hash_table), (IPFS,
possibly). All this remote data is still protected by the capabilites-based
security framework.

## Package management

Once the the DHT infrastructure is 

## Related projects and inspirations

- https://tomaka.medium.com/the-future-of-operating-systems-efa31e17d66d

## FAQ

### Q: Looks like FushiaOS, isn't it?

#### A: Probably yes. Fushia is a full OS, including hardware driver, task management,
memory management and so on. That's a lot of work we don't want to do here.

On the userland aspect, it's also a capablities based OS, that aims to
protect os security, the user's data and privacy.

My main grip is that Fushia is developed by a company whose main business
model is to monetize as much as they can the user data they stole. This
impedence mismatch makes me very reluctant to use anything that comes out
of their labs, regardless of the technical merits of the solutions.
