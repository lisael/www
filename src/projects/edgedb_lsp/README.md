Edgdb LSP
=========

Building a LSP server for [Edge|DB](https://www.edgedb.com/) in rust.

# Software stack

The plan is to use [chumsky](https://crates.io/crates/chumsky) to build a 
[rowan](https://crates.io/crates/rowan) red-green tree of
the syntax and iterate on top of this to provide completion, linting and
hints. We will probably use [lspower](https://crates.io/crates/lspower)
as LSP framework.
