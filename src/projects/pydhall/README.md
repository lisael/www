# PyDhall

[PyDhall](https://gitlab.com/lisael/pydhall) is a python implementation
of the [Dhall configuration language](https://dhall-lang.org/).
