# Projects

Here's a (very incomplete) list of programming projects of mine. This
is not intended to show the full documentation of the projects, but
rather notes and design issues.

- [KakFM](./kakfm.md) A kakoune-like file manager
- [Fastidious](./fastidious/) A python PEG parser generator
- [Literate MdBook](./literate_mdbook/) Include versioned snippets from a git
  repo in a mdBook. 
- [Rebor](./rebor.md) A modern modal editor in Rust (TermUI and GUI with [druid](https://pydantic-docs.helpmanual.io/usage/models/))
- [Santa](./santa.md) A git-friendly declarative JSON/REST API test runner. 
- [PyDhall](./pydhall/) a python implementation
  of the [Dhall configuration language](https://dhall-lang.org/).
- [Sithrax](./sithrax/) A capabilities-based runtime to serve as a an OS
  on the top of Linux on mobile devices. 
<!-- - [Eris](./lethe/eris/) A capablities-enable programming language that
  compile to wasm to develop applications on Sithrax. -->

Misc experiments and small-ish projects:

- [PyQBE](https://gitlab.com/lisael/pyqbe) is a python builder for [QBE](https://c9x.me/compile/)
  [intermediate language](https://c9x.me/compile/doc/il.html). Create machine code on the
  fly from python.
- [HyPEG](https://gitlab.com/lisael/hypeg) A PEG parser generator in [Hy](https://github.com/hylang),
  a Lisp frontend to python. This relied too much on recursive macros to generate python AST,
  and lead to unbearable compile times. Was fun, though :)
