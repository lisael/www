# KakFM

KakFM is a kakoune-like file manager. By kakoune-like I meant that it
follows kakoune philosophy and configuration idioms.

## Philosophy

KakFM takes for granted the premises of Kakoune: a tool that does one thing
correctly (displaying and interacting with a file tree) and a way to configure
and extend it using the power of UNIX abstarctions.

Kakoune also emphasis on discoverability and online help. KakFM has this goal
too, but it's not there yet.

## Configuration

The configuration is inspires and tries to mimic kak's command language:

- Each configuration line is a command
- The configuration language provides ways to open shells (sh and python) and
  to optionally interpret the resulting output.

A configuration file should (almost) be no different than a Kakoune config (
the only addition at the moment is the `%py{}` sigil -- more on that below ––)

## I know all that. Could you be more precise?

OK, so you know Kakoune and it's configuration? Then those things must
work in KakFM as you'd expect in Kakoune:

### Builtin commands

- `define-command [-params <range>] %{...}`
- `evaluate-commands %{...}`
- `echo %{}`
- `nop`

#### Expected

- `declare-option`
- `set-option`
- `map`
- `execute-keys`

### Sigils

Just like Kakoune, KakFM accept `%<brackets>` strings where brackets are
`[]`, `()`, `{}`, `<>`. Adding a sigil changes the meaning of a `%` string

#### Expected

- `%sh{}` -- open a shell (`/bin/sh`)
- `%opt{}` -- replace by the string value of an option (declared with `declare-option`)
- `%val{}` -- Expose interals and builtin states

#### Additions

- `%py{}`
    - KakFM is a Python program, the Python integration is trivial, I just
      couldn't resist
    - Arguments in user defined commands are injected as `kakfm_arg_<N>` just as
      how `$N` arguments are injected in `%sh{}` sigils
