from typing import Callable
from functools import partial


class Event:
    def __init__(self, name: str):
        self.name: str = name
        self.callbacks: set[Callable] = set()

    def add_callback(self, cb: Callable):
        self.callbacks.add(cb)
        return self

    def rm_callback(self, cb: Callable):
        self.callbacks.remove(cb)
        return self

    def __call__(self, *args, **kwargs):
        for cb in self.callbacks:
            cb(*args, **kwargs)


class EventManager:
    events: dict[str, Event] = {}

    @classmethod
    def declare_event(cls, name: str) -> Event:
        try:
            return cls.events[name]
        except KeyError:
            cls.events[name] = Event(name)
            return cls.events[name]

    @classmethod
    def send_event(cls, name, *args, **kwargs):
        try:
            cb = cls.events[name]
        except KeyError:
            return
        cb(*args, **kwargs)

    @classmethod
    def listen(cls, name: str, fn: Callable) -> Callable:
        evt = cls.declare_event(name)
        evt.add_callback(fn)
        # this may be used as a decorator, so we must return
        # the function.
        return fn

    @classmethod
    def unregister(cls, name: str, fn: Callable):
        evt = cls.declare_event(name)
        evt.rm_callback(fn)


# Neat way to create a paramterized decorator out of a function
def listen(name: str) -> Callable:
    return partial(EventManager.listen, name)


send_event = EventManager.send_event
