import pytest


# chunk begin test_raw_listen
from .events import EventManager
# chunk begin_ellipsis test_raw_listen
# chunk begin test_listen_decorator
from .events import listen, send_event
# chunk begin_ellipsis test_listen_decorator

# chunk end_ellipsis test_raw_listen
@pytest.fixture
def manager():
    old_events = EventManager.events
    EventManager.events = {}
    yield EventManager
    EventManager.events = old_events


def test_send_event(manager):
    manager.declare_event("a")
    call_args = []

    def save_args(*args, **kwargs):
        call_args.append((args, kwargs))

    manager.listen("a", save_args)
    send_event("a", 1, aa=1)
    assert call_args == [((1,), {"aa": 1})]
# chunk end test_raw_listen


# chunk end_ellipsis test_listen_decorator
def test_listen(manager):
    manager.declare_event("a")
    call_args = []

    @listen("a")
    def save_args(*args, **kwargs):
        call_args.append((args, kwargs))

    send_event("a", 1, aa=2)
    assert call_args == [((1,), {"aa": 2})]
# chunk end test_listen_decorator
