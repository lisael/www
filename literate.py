"""
Copyright © 2021 Bruno Dupuis <lisael@lisael.org>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar:


        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
"""

from __future__ import annotations

from typing import cast, List, Optional, Mapping, Any, Callable, Tuple, Union
import logging
import sys
import json
from pprint import pformat
from dataclasses import dataclass, field, asdict
from io import StringIO
import re
from os.path import basename
from subprocess import getstatusoutput

# chunk begin cache_import
try:
    from functools import cache
except ImportError:
    # python3.7 support
    from functools import lru_cache

    def cache(user_function):
        'Simple lightweight unbounded cache.  Sometimes called "memoize".'
        return lru_cache(maxsize=None)(user_function)
# chunk end cache_import


log = logging.getLogger()


LITERATE_DIRECTIVE_RE = re.compile(r"^\s*<!--\s*(literate:.*)-->$")
INDENT_RE = re.compile(r"^(\s*)")

COMMENT_REGEX_TYPE = {
    "#": ("sh", "py", "ini", "kak", "toml", "yaml"),
    "//": ("cpp", "rs", "js", "h", "c"),
    "--": ("hs", "sql"),
    ";;": ("ini", "emacs", "lisp"),
    '"': ("vim"),
    "/**/": ("cpp", "rs", "js", "h", "c", "css"),
    "<!---->": ("html", "md", "xml"),
}

COMMENT_REGEX = {
    "#": re.compile(r"^\s*#\s*chunk (.*)$"),
    "//": re.compile(r"^\s*//\s*chunk (.*)$"),
    "--": re.compile(r"^\s*--\s*chunk (.*)$"),
    ";;": re.compile(r"^\s*;;\s*chunk (.*)$"),
    '"': re.compile(r'^\s*"\s*chunk (.*)$'),
    "/**/": re.compile(r"^\s*/\*\s*chunk (.*)\*/$"),
    "<!---->": re.compile(r"^\s*<!--\s*chunk (.*)-->$"),
}

COMMENT_FACTORY = {
    "#": lambda s: f"# {s}",
    "//": lambda s: f"// {s}",
    "--": lambda s: f"-- {s}",
    ";;": lambda s: f";; {s}",
    '"': lambda s: f'" {s}',
    "/**/": lambda s: f"/* {s} */",
    "<!---->": lambda s: f"<!-- {s} -->",
}

LANG_FOR_EXT = {}


@cache
def extension(path: str) -> Optional[str]:
    parts = basename(path).split(".")
    if len(parts) == 1:
        return None
    return parts[-1]


@cache
def get_lang_for_ext(ext: Optional[str]) -> Optional[str]:
    return LANG_FOR_EXT.get(ext, ext)


@cache
def get_comment_types_for_lang(lang: str) -> List[str]:
    return [k for k, v in COMMENT_REGEX_TYPE.items() if lang in v]


@cache
def make_comment(cmt: str, lang: str) -> str:
    cmt_types = get_comment_types_for_lang(lang)
    if cmt_types:
        cmt_type = cmt_types[0]
    else:
        cmt_type = "#"
    return COMMENT_FACTORY[cmt_type](cmt)

@cache
def get_regexes_for_lang(lang: Optional[str]) -> List[re.Pattern]:
    types = get_comment_types_for_lang(lang)
    if types:
        return [COMMENT_REGEX[t] for t in types]
    return list(COMMENT_REGEX.values())


def get_fence_comment(line: str, lang: Optional[str]) -> Optional[str]:
    if "chunk" not in line:
        return None
    regexes = get_regexes_for_lang(lang)
    for r in regexes:
        match = r.match(line)
        if match:
            return match.group(1)
    else:
        return None


def get_indent(line: str) -> str:
    return cast(re.Match, INDENT_RE.match(line)).groups()[0]


KWARGS_RE = re.compile(r":[^:]*:")


def parse_directive_args(definition: str) -> Tuple[
        List[str],
        Mapping[str, str]]:
    kwargs = {}
    kws = KWARGS_RE.findall(definition)
    kws = [k.strip(":") for k in kws]
    for kw in kws:
        try:
            key, value = kw.split("=", maxsplit=1)
        except ValueError:
            log.warning(
                f"Can't parse configuration `{kw}`. Missing `=` ?")
        else:
            kwargs[key] = value
    args = KWARGS_RE.split(definition)
    args = [a.strip() for a in args]
    args = [a for a in args if a]
    return (args, kwargs)


def get_literate_directive(line: str) -> Optional[Directive]:
    m = LITERATE_DIRECTIVE_RE.match(line)
    if m is None:
        return None
    try:
        d = Directive.from_str(m.group(1))
        if isinstance(d, VerbatimDirective):
            d.content = "\n" + line.replace("literate: !", "literate:", 1)
        return d
    except Exception:
        raise
        return None


class Directive:
    @classmethod
    def from_str(cls, definition: str) -> Directive:
        assert definition.startswith("literate: ")
        parts = definition.split(maxsplit=2)
        dir = parts[1]
        if dir == "set":
            return SetDirective.from_str(parts[-1])
        elif dir == "chunk":
            return ChunkDirective.from_str(parts[-1])
        elif dir == "tree":
            return TreeDirective.from_str(parts[-1])
        if dir == "!":
            return VerbatimDirective()
        else:
            log.warning(f"Directive `{dir}` unknown. Ignoring")
            return NoOpDirective()


class VerbatimDirective(Directive):
    def __init__(self, content: str = ""):
        self.content = content

    def __call__(self, output: StringIO, visitor: Visitor):
        output.write(self.content)


class TreeDirective(Directive):
    def __init__(self,
                 repo: Optional[str] = None,
                 rev: Optional[str] = None,
                 path: Optional[str] = None,
                 depth: Optional[str] = None,
                 hidden: Optional[str] = None,
                 ):
        self.repo = repo
        self.rev = rev
        self.path = path
        self.depth = int(depth) if depth is not None else 9999
        self.show_hidden = hidden == "yes"

    @classmethod
    def from_str(cls, definition):
        args, kwargs = parse_directive_args(definition)
        return cls(*args, **kwargs)

    def filter(self, files) -> List[str]:
        result = []
        path = self.path.rstrip("/") + "/"
        path_last = path.split("/")[-2]
        for f in files:
            # git gives absolute paths
            if f.startswith(path):
                f = path_last + "/" + f[len(path):]
            parts = f.split("/")
            if len(parts) > self.depth:
                continue
            if not self.show_hidden:
                if any([p.startswith(".") for p in parts]):
                    continue
            if self.path == ".":
                f = "./" + f
            result.append(f)
        return result

    def __call__(self, output: StringIO, visitor: Visitor):
        r = self.repo if self.repo else visitor.repo
        repo = cast(Repo, visitor.config.repos[r])
        path = self.path if self.path else visitor.path
        rev = self.rev if self.rev else visitor.rev
        files = repo.get_files_at(rev, path)
        files = self.filter(files)
        output.write("\n```")
        output.write(TreeRenderer(files).render())
        output.write("```\n")


class ChunkDirective(Directive):
    def __init__(self, name: str,
                 repo: Optional[str] = None,
                 rev: Optional[str] = None,
                 path: Optional[str] = None,
                 lang: Optional[str] = None,
                 start_line: Optional[str] = None,
                 end_line: Optional[str] = None,
                 title: Optional[str] = None,
                 raw: str = "no",
                 ):
        self.name = name
        self.repo = repo
        self.rev = rev
        self.path = path
        self.lang = lang
        self.title = title
        self.raw = raw == "yes"
        self.start_line = int(start_line) if start_line is not None else None
        self.end_line = int(end_line) if end_line is not None else None
        if name == "ALL":
            self.start_line = 0
            self.end_line = 10**6
        assert (
            (start_line is None and end_line is None)
            or (start_line is not None and end_line is not None)
            ), "start_line implies end_line"

    # chunk begin my_wip_chunk
    @classmethod
    def from_str(cls, definition):
        args, kwargs = parse_directive_args(definition)
        return cls(*args, **kwargs)
    # chunk end my_wip_chunk

    def write_line_nr_chunk(self, content: str, output: StringIO):
        assert self.start_line is not None
        assert self.end_line is not None
        for idx, line in enumerate(content.splitlines()):
            if idx >= self.start_line:
                if idx > self.end_line:
                    return
                output.write(line)
                output.write("\n")

    def write_fenced_chunk(self, content: str, output: StringIO, lang: Optional[str]) -> int:
        started = False
        start_line = -1
        ellipsis_len = -1
        for line_nr, line in enumerate(content.splitlines()):
            fence = get_fence_comment(line, lang)
            if fence is None:
                if started:
                    if ellipsis_len == -1:
                        output.write(line)
                        output.write("\n")
                    else:
                        ellipsis_len += 1
                continue
            else:
                action, name = fence.strip().split()
                if name != self.name:
                    continue
                elif action.lower() == "begin":
                    if start_line == -1:
                        start_line = line_nr + 2
                    started = True
                elif action.lower() == "end":
                    started = False
                elif action.lower() == "begin_ellipsis":
                    ellipsis_len = 0
                elif action.lower() == "end_ellipsis":
                    output.write("\n")
                    output.write(get_indent(line))
                    output.write(make_comment(f"... ({ellipsis_len} lines)", lang))
                    output.write("\n\n")
                    ellipsis_len = -1
                continue
        return start_line

    def get_lang(self, path) -> Optional[str]:
        if self.lang is not None:
            if self.lang.lower() == "none":
                return None
            return self.lang
        return get_lang_for_ext(extension(path))

    # chunk begin with_ellipsis
    def __call__(self, output: StringIO, visitor: Visitor):
        r = self.repo if self.repo else visitor.repo
        repo = cast(Repo, visitor.config.repos[r])
        path = self.path if self.path else visitor.path
        rev = self.rev if self.rev else visitor.rev
        title = self.title if self.title else visitor.title
        line_nr = 1
        lang = self.get_lang(path)

        temp = StringIO()
        # chunk begin_ellipsis with_ellipsis
        content = repo.get_content_at(rev, path)
        if self.start_line is not None:
            self.write_line_nr_chunk(content, temp)
            line_nr = self.start_line + 1
        else:
            line_nr = self.write_fenced_chunk(content, temp, lang)

        if title == "yes" and repo.title_format:
            title = repo.title_format.format(path=path, rev=rev, line_nr=line_nr)
            output.write("\n")
            output.write(title)
        # chunk end_ellipsis with_ellipsis

        if not self.raw:
            output.write("\n``````")
            if lang:
                output.write(lang + "\n")
        # chunk begin_ellipsis with_ellipsis
        output.write(temp.getvalue())
        # output.write("\n")
        # chunk end_ellipsis with_ellipsis
        if not self.raw:
            output.write("``````")
        # chunk end with_ellipsis


class SetDirective(Directive):
    def __init__(self,
                 repo: Optional[str] = None,
                 rev: Optional[str] = None,
                 path: Optional[str] = None,
                 title: Optional[str] = None
                 ):
        self.repo = repo
        self.rev = rev
        self.path = path
        self.title = title

    @classmethod
    def from_str(cls, definition):
        definition = definition.strip(" :")
        parts = definition.split(":")
        parts = [p.strip() for p in parts]
        parts = [p for p in parts if p]
        config = {}
        for part in parts:
            try:
                key, value = part.split("=", maxsplit=1)
            except ValueError:
                log.warning(
                    f"Can't parse configuration `{part}`. Missing `=` ?")
            else:
                config[key] = value
        return cls(**config)

    def __call__(self, output: StringIO, visitor: Visitor):
        for key in ["repo", "rev", "path", "title"]:
            val = getattr(self, key)
            if val is not None:
                setattr(visitor, key, val)


class NoOpDirective(Directive):
    def __call__(self, output: StringIO, visitor: Visitor):
        pass


@dataclass
class Chapter:
    name: str
    content: str
    number: Optional[List[int]]
    sub_items: List[Mapping]
    path: Optional[str]
    source_path: Optional[str]
    parent_names: List[str]
    children: List[Chapter] = field(init=False)

    def __post_init__(self):
        self.children = [
            Chapter(**c["Chapter"])
            for c in self.sub_items
            if "Chapter" in c]

    def as_dict(self):
        children = self.children
        self.children = []
        d = asdict(self)
        for item in d["sub_items"]:
            if "Chapter" in item:
                item["Chapter"] = children.pop(0).as_dict()
        return d


from pathlib import Path


@dataclass
class Repo:
    path: str = field()
    root: Path
    git_root: Path = field(init=False)
    title_format: str = "{path}"

    def __post_init__(self):
        self.root = Path(self.root)
        self.git_root = self.root / self.path

    def __hash__(self):
        return id(self)

    @cache
    def get_content_at(self, rev: str, path: str) -> str:
        if rev == "WIP":
            log.warning(":rev=WIP: is not portable as it may include uncommited changes")
            with open(self.git_root / path) as f:
                return f.read()
        # log.debug(f"git -C {self.git_root} show {rev}:{path}")
        status, content = getstatusoutput(f"git -C {self.git_root} show {rev}:{path}")
        if status != 0:
            log.warning(content)
            return ""
        return content

    @cache
    def get_files_at(self, rev: str, path: str) -> List[str]:
        assert rev != "WIP", "Can't use WIP for tree revs, commit the files and use HEAD"
        # log.debug(f"git -C {self.git_root} show {rev}:{path}")
        if path == ".":
            path = ""
        status, content = getstatusoutput(f"git -C {self.git_root} ls-tree -r -z --name-only {rev} {path}")
        if status != 0:
            log.warning(content)
            return []
        return content.split("\0")

@dataclass
class Config:
    root: str = field()
    repos: Mapping[str, Union[str, Mapping, Repo]] = field(default_factory=dict)
    default_repo: Optional[str] = field(default=None)

    def __post_init__(self):
        for k, v in list(self.repos.items()):
            if isinstance(v, str):
                self.repos[k] = Repo(path=v, root=self.root)
            elif isinstance(v, dict):
                v.setdefault("root", self.root)
                self.repos[k] = Repo(**v)

    def get_default_repo(self):
        if self.default_repo is not None:
            return self.repos[self.default_repo]


class Visitor:
    def __init__(self, conf: Config):
        self.config = conf
        self.repo = conf.get_default_repo()
        self.rev = None
        self.path = None
        self.title = "yes"

    def save_state(self) -> Visitor:
        v = Visitor(self.config)
        v.repo = self.repo
        v.rev = self.rev
        v.path = self.path
        v.title = self.title
        return v

    def restore_state(self, state: Visitor):
        self.repo = state.repo
        self.rev = state.rev
        self.path = state.path
        self.title = state.title

    def visit(self, c: Chapter):
        state = self.save_state()
        self.visit_chapter(c)
        for c in c.children:
            self.visit(c)
        self.restore_state(state)

    def visit_chapter(self, c: Chapter):
        content = StringIO()
        for line in c.content.splitlines():
            directive = get_literate_directive(line)
            if directive is None:
                content.write("\n")
                content.write(line)
            else:
                d = cast(Callable, directive)
                d(content, self)
        c.content = content.getvalue()


from collections import defaultdict

def Tree():
    return defaultdict(Tree)


class TreeRenderer:
    def __init__(self, files: List[str]):
        self.files = files

    def render(self) -> str:
        files = sorted(self.files)
        # build the file tree
        tree = Tree()
        for f in files:
            # isdir = f.endswith("/")
            f = f.strip("/")
            parts = f.split("/")
            # if isdir:
            #     parts[-1] += "/"
            t = tree
            for p in parts:
                t = t[p]

        def render_tree(t, output, prefix=None):
            if not t:
                output.write("\n")

            last = len(t) - 1
            for idx, (k, v) in enumerate(t.items()):
                if prefix is not None:
                    if idx != last:
                        pf = prefix + "├── "
                    else:
                        pf = prefix + "└── "
                else:
                    pf = ""
                output.write(pf)
                output.write(k + "\n")
                if v:
                    if prefix is not None:
                        if idx != last:
                            pf = prefix + "│   "
                        else:
                            pf = prefix + "    "
                    else:
                        pf = ""
                    render_tree(v, output, pf)

        output = StringIO()
        render_tree(tree, output)
        val = output.getvalue()
        return val


def set_logger(level=logging.DEBUG):
    global log
    log = logging.getLogger("literate.py")
    log.setLevel(level)

    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        '%(asctime)s [%(levelname)s] (%(name)s): %(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)


def supports(renderer: str) -> int:
    return 0


DEFAULT_CONF: Mapping = {"repos": {}}


def preprocess(config: Mapping, content: Mapping) -> Mapping:
    # log.debug(pformat(config))
    chapters = [
        Chapter(**c["Chapter"])
        for c in content["sections"]
        if "Chapter" in c]
    conf = config["config"].get(
            "preprocessor", {}).get("literate", DEFAULT_CONF)
    conf = {"repos": conf["repos"]}
    conf["root"] = config["root"]
    visitor = Visitor(Config(**conf))
    for c in chapters:
        visitor.visit(c)
    for s in content["sections"]:
        if "Chapter" in s:
            s["Chapter"] = chapters.pop(0).as_dict()
    return content


if __name__ == "__main__":
    set_logger()
    if len(sys.argv) > 1 and sys.argv[1] == "supports":
        sys.exit(supports(sys.argv[2]))
    else:
        data = json.load(sys.stdin)
        result = preprocess(data[0], data[1])
        json.dump(result, sys.stdout)
