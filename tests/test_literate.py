import pytest
from textwrap import dedent

import sys
sys.path.append(".")

from literate import TreeRenderer

def test_tests():
    assert 1 + 1 == 2


def test_tree():
    r = TreeRenderer([
        "aa/",
        "aa/bb/",
        "aa/bb/dd/",
        "aa/cc",
    ])
    assert r.render() == dedent("""\
        aa
        ├── bb
        │   └── dd
        └── cc
        """)
